# README #

This is an docker image for Alpine Linux base images with Apache2 and PHP7 running.

### What is this repository for? ###

* This repo is for the base image of Application/Service
* Other packages included for this image are;
    * apache2
    * apache2-utils
    * ca-certificates
    * libxml2-dev
    * libressl
    * openssl
    * php7
    * php7-apache2
    * php7-apcu
    * php7-curl
    * php7-intl
    * php7-json
    * php7-mbstring
    * php7-mcrypt
    * php7-openssl
    * php7-phar
    * php7-soap
    * php7-xmlrpc
    * su-exec
    * confd
    * new relic

### How do I get set up? ###

* Summary of set up
    * To test if the images is working, do the temporary editing on the Dockerfile before build and run process;
        * Comment the statement that removes the default file by adding a sharp sign at the beginning of the statement. 
        ** #RUN rm /var/www/localhost/htdocs/index.html **
        * update the last statement **CMD rm -rf /run/apache2/* || true && /usr/sbin/httpd -DFOREGROUND**
* Execute command to create the docker image: ***docker build --no-cache -t usap-base-img .***
* Execute command to make the image up and running: ***docker run -d -p 80:80 --name usap-base usap-base-img***
* Enter http://localhost on your browser

### Who do I talk to? ###

* Fernan Cambronero