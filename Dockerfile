FROM alpine:3.6

LABEL maintainer="fcambronero@usautoparts.com" \
      description="This Image will be use as the base image of the application" \
      version="1.0"

ENV DOCKER_SETUP_DIR /tmp/docker-setup
ENV CONFD_VERSION 0.14.0
ENV CONFD_SHA 36c87bcf39bb5348f13aa90289c3e294f2361d0f3dd3cf16a8d0ce2c337e230f

# Install any needed packages specified in requirements.txt
COPY ./packages/requirements.txt $DOCKER_SETUP_DIR/packages/requirements.txt

    # Run an update and install all basic required packages
RUN set -ex \
    && apk update --no-cache \
    && apk upgrade --no-cache \
    && apk add --no-cache --virtual .curl-deps \
       curl \
    && apk add --no-cache --update `cat $DOCKER_SETUP_DIR/packages/requirements.txt` \
    #Install Confd
    && wget --no-check-certificate -O /usr/bin/confd https://github.com/kelseyhightower/confd/releases/download/v${CONFD_VERSION}/confd-${CONFD_VERSION}-linux-amd64 \
    && chmod +x /usr/bin/confd \
    && echo "${CONFD_SHA}  /usr/bin/confd" | sha256sum -c - \
    # Create and Change Directory Permission for confd feature
    && mkdir -p /etc/confd/conf.d \
    && mkdir -p /etc/confd/templates \
    # Create a directory and change the permission of apache2
    && mkdir -p /run/apache2 \
    && chmod -R 775 /run/apache2 \
    # Chmod for SU-EXEC
    && chmod u+s /sbin/su-exec \
    # Prepare required directories for Newrelic installation
    && mkdir -p /var/log/newrelic /var/run/newrelic \
    && touch /var/log/newrelic/php_agent.log /var/log/newrelic/newrelic-daemon.log \
    && chmod -R g+ws /tmp /var/log/newrelic/ /var/run/newrelic/ \
    && chown -R 1001:0 /tmp /var/log/newrelic/ /var/run/newrelic/ \
    # Download and install Newrelic binary
    && export NEWRELIC_VERSION=$(curl -sS https://download.newrelic.com/php_agent/release/ | sed -n 's/.*>\(.*linux-musl\).tar.gz<.*/\1/p') \
    && cd /var \
    && curl -sS "https://download.newrelic.com/php_agent/release/${NEWRELIC_VERSION}.tar.gz" | gzip -dc | tar xf - \
    && cd "${NEWRELIC_VERSION}" \
    && NR_INSTALL_SILENT=true ./newrelic-install install \
    && rm -f /var/run/newrelic-daemon.pid \
    && rm -f /var/.newrelic.sock \
    && apk del .curl-deps \
    # Clean up process
    && rm -rf /var/cache/apk/* \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /run/apache2/* \
    && rm -rf /tmp/* \
    # Removed the default content
    && rm /var/www/localhost/htdocs/index.html
